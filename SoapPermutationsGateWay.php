<?php
/**
 * /classes/SoapPermutationsGateWay.php
 */

class SoapPermutationsGateWay {

    // Utility function to find factorial of n.
    protected static function _factorial($n) {
        $n = intval($n);
        $fact = 1;
        for ($i = 1; $i <= $n; $i++) {
            $fact *= $i;
        }
        return $fact;
    }

    public function countPermutations($messageData){
        //logging
        /*$rawPost  = "Input:\r\n";
        $rawPost .= file_get_contents('php://input');
        $rawPost .= "\r\n---\r\nmessageData:\r\n";
        $rawPost .= serialize($messageData);
        file_put_contents("log.txt",$rawPost);*/

        //config file for db connection
        $config = require_once(__DIR__.'./includes/config.php');
        $connection = false;
        if (isset($config['database']['adapter']) && $config['database']['adapter'] == 'Mysql') {
            $serverName = $config['database']['host'];
            $userName = $config['database']['username'];
            $password = $config['database']['password'];
            $dbName = $config['database']['dbname'];
            try {
                $connection = new PDO("mysql:host=$serverName;dbname=$dbName", $userName, $password);
                // set the PDO error mode to exception
                $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(PDOException $e) {
                echo "DataBase Connection failed: " . $e->getMessage();
            }
        }

        $answerList = new ArrayObject();
        if (isset($messageData->messageList->Message) && is_array($messageData->messageList->Message)) {
            foreach ($messageData->messageList->Message as $index => $message) {

                if (preg_match('/^(\w+)$/',$message,$matches) && strlen($message) <= 8) {

                    //start time
                    $time_pre = microtime(true);

                    $param = $matches[1];
                    try {
                        //check if we already counted permutations for this message
                        $select = "SELECT result FROM permutations WHERE param='$param'";
                        $request = $connection->prepare($select);
                        $request->execute();
                        // set the resulting array to associative
                        $dbData = $request->setFetchMode(PDO::FETCH_ASSOC);
                        $answer = $request->fetch();
                        if (isset($answer['result'])) {
                            //needed data is found
                            $answerList[$param]['counter'] = intval($answer['result']);
                        } else {
                            //there is no data in DB, do calculate

                            //char frequency
                            $freq = array();
                            $fact = 1;

                            $length = strlen($param);
                            $inputArray = str_split($param);
                            foreach ($inputArray as $char) {
                                if (isset($freq[$char])) {
                                    $freq[$char]++;
                                } else {
                                    $freq[$char] = 1;
                                }
                            }

                            foreach ($freq as $key => $value) {
                                $fact = $fact * self::_factorial($value);
                            }

                            $counter = self::_factorial($length) / $fact;
                            $answerList[$param]['counter'] = $counter;
                            //insert new message and counter in DB
                            $insert = "INSERT INTO permutations (param,result) VALUES ('$param',$counter)";
                            $request = $connection->prepare($insert);
                            $request->execute();
                        }
                    } catch (PDOException $e) {
                        echo $e->getMessage();
                    }

                    $time_post = microtime(true);
                    $exec_time = $time_post - $time_pre;
                    $answerList[$param]['time'] = $exec_time;
                }
            }
        }

        return array(
            'answerList' => $answerList
        );
    }
}