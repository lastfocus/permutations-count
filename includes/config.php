<?php

return array(
    'database' => [
        'adapter' => 'Mysql',
        'host'     => '127.0.0.1',
        'username' => 'YourUsername',
        'password' => 'YourPassword',
        'dbname'   => 'YourDataBaseName',
    ],
);