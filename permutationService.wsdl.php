<?php
/*
 * permutationService.wsdl.php
 */
header("Content-Type: text/xml; charset=utf-8");
echo  "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
?>
<definitions name = "permutationsService"
             xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
             xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"
             xmlns:tns="http://<?=$_SERVER['HTTP_HOST']?>/permutationService.wsdl.php"
             xmlns:xs="http://www.w3.org/2001/XMLSchema"
             xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/"
             xmlns:http="http://schemas.xmlsoap.org/wsdl/http/"
             xmlns="http://schemas.xmlsoap.org/wsdl/">

    <types>
        <xs:schema elementFormDefault="qualified"
                   xmlns:tns="http://schemas.xmlsoap.org/wsdl/"
                   xmlns:xs="http://www.w3.org/2001/XMLSchema"
                   targetNamespace="http://<?=$_SERVER['HTTP_HOST']?>/permutationService.wsdl.php">
            <xs:element name="Message" type="xs:string"/>
            <xs:complexType name="MessageList">
                <xs:sequence>
                    <xs:element name="message" type="Message" minOccurs="1" maxOccurs="unbounded" />
                </xs:sequence>
            </xs:complexType>
            <xs:element name="Request">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="messageList" type="MessageList" />
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="Answer">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="counter" type="xs:integer" />
                        <xs:element name="time" type="xs:string" />
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="AnswerList">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="answer" type="Answer" minOccurs="1" maxOccurs="unbounded" />
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="Response">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="answerList" type="AnswerList" />
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:schema>
    </types>

    <message name = "CountPermutationsRequest">
        <part name="Request" element="tns:Request" />
    </message>

    <message name = "CountPermutationsResponse">
        <part name="Response" element="tns:Response" />
    </message>

    <portType name = "PermutationsServicePortType">
        <operation name = "countPermutations">
            <input message = "tns:CountPermutationsRequest"/>
            <output message = "tns:CountPermutationsResponse"/>
        </operation>
    </portType>

    <binding name = "PermutationsServiceBinding" type = "tns:PermutationsServicePortType">
        <soap:binding style = "rpc"
                      transport = "http://schemas.xmlsoap.org/soap/http"/>
        <operation name = "countPermutations">
            <soap:operation soapAction = "countPermutations"/>
            <input>
                <soap:body
                    encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/"
                    use = "encoded"/>
            </input>

            <output>
                <soap:body
                    encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/"
                    use = "encoded"/>
            </output>
        </operation>
    </binding>

    <service name = "Permutations_Service">
        <documentation>WSDL File for PermutationsService</documentation>
        <port binding = "tns:PermutationsServiceBinding" name = "PermutationsServicePort">
            <soap:address
                location = "http://<?=$_SERVER['HTTP_HOST']?>/permutationsService.php" />
        </port>
    </service>
</definitions>