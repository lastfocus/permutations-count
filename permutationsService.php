<?php
/**
 * permutationsService.php
 */
header("Content-Type: text/xml; charset=utf-8");
header('Cache-Control: no-store, no-cache');
header('Expires: '.date('r'));

/**
 ** Функция для автозагрузки необходимых классов
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

ini_set("soap.wsdl_cache_enabled", "0"); // отключаем кеширование WSDL-файла для тестирования

//Создаем новый SOAP-сервер
$server = new SoapServer("http://{$_SERVER['HTTP_HOST']}/permutationService.wsdl.php");
//Регистрируем класс обработчик
$server->setClass("SoapPermutationsGateWay");
//Запускаем сервер
$server->handle();