<?php
/**
 * /soapClient.php
 */
header("Content-Type: text/html; charset=utf-8");
header('Cache-Control: no-store, no-cache');
header('Expires: '.date('r'));


/**
 ** Функция для автозагрузки необходимых классов
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);

// Заготовки объектов
class Request{
    public $messageList;
}

// создаем объект для отправки на сервер
$req = new Request();
$req->messageList = new \ArrayObject();

$msg1 = 'meessage';
$msg2 = 'drgsjk56';
$msg3 = 'aba';
$msg4 = 'abaddsvv';

$soap_msg1 = new \SoapVar($msg1, XSD_STRING, null, null, 'Message');
$soap_msg2 = new \SoapVar($msg2, XSD_STRING, null, null, 'Message');
$soap_msg3 = new \SoapVar($msg3, XSD_STRING, null, null, 'Message');
$soap_msg4 = new \SoapVar($msg4, XSD_STRING, null, null, 'Message');

$req->messageList->append($soap_msg1);
$req->messageList->append($soap_msg2);
$req->messageList->append($soap_msg3);
$req->messageList->append($soap_msg4);

$client = new SoapClient(
    "http://{$_SERVER['HTTP_HOST']}/permutationService.wsdl",
    array(
        'soap_version' => SOAP_1_2,
        'trace'=>1,
        'cache_wsdl'=>WSDL_CACHE_NONE
    )
);

if (isset($_GET['param'])) {
    $param = $_GET['param'];
} else {
    $param = 'test';
}
echo '<pre>';
var_dump($client->countPermutations($req));